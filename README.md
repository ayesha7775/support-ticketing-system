# Support Ticketing System

* Support ticketing system is a customer service tool that allows to efficiently provide service to customers by assigning a ticket to every service inquiry. One can use it to track the issue to any service inquiry.

# Basic structure of STS using FLASK and API

## File
* `__init__`.py to create an instance of Flask class and to set the app configuration
* views.py to tell Flask what URL should trigger the function
* app.py to run the app and see it in action
* model.py to create the database schema

## Folder
* venv folder by setting up the virtual environment for the application
* app folder to store static files, templates, `__init__`.py, model.py, database.db and views.py files
* static folder for css, js and img repository
* templates folder for html pages that need to render in views

## Command to push in gitlab repository
* git init
* git add .
* git commit -m "message"
* git remote add origin "url"
* git push origin main

# System Analysis

## Super Admin
* can login to the system
* can add user
* can edit user role
* can view all user info
* can add a ticket
* can answer a ticket
* can view all user tickets
* can view all replied messages against a ticket_id
### Flowchart
![s1](/uploads/93324864aeb6941fbdbfc35b744999eb/s1.png)

## Support Admin
* can login to the system
* can add user
* can view all user info
* can add a ticket
* can answer a ticket
* can view all user tickets
* can view all replied messages against a ticket_id
### Flowchart
![s2](/uploads/264de78ab6f77974c5dc2b8c3696f7c6/s2.png)

## Support Person
* can login to the system
* can answer a ticket
* can view all user tickets
* can view all replied messages against a ticket_id\
### Flowchart
![s3](/uploads/dd028e791d2b673d1de1bea77af25579/s3.png)

## Customer
* can register
* can login to the system
* can add a ticket
* can answer a ticket
* can view own tickets
* can view all replied messages against a ticket_id
### Flowchart
![s4](/uploads/4870c34f627b980c2a997b70768f01e3/s4.png)


## Class Diagram

![s5](/uploads/537e89a2d1850f7c336559126b86f7f9/s5.png)

## Entity Relationship Diagram

![s6](/uploads/3568e17452ad7170bb026ac20ea7e719/s6.png)


## Super Admin Panel

__Login:__\
API Endpoint: http://127.0.0.1:5000/login \
Request data: Using basic authentication form
````
  Username
  Password
  ````
Response:  
````
  If succeed: {'token': token_value}, 200
  ````
````
  If failed: {'message': 'Authorization is required'}, 400
  {'message': 'Username is required'}, 400
  {'message': 'Password is required'}, 400
  {'message': 'Please enter valid username and password'}, 400
  ````
__Add user:__\
API Endpoint: http://127.0.0.1:5000/register \
Request data:
````
  { "name": "", "email": "", "address": "", "username": "", "password": "", "confirm_password": "" }
  ````
Response:
````
  If succeed: { "email": "", "username": ""}, 200
  ````
````
  If failed: {'message': 'Request body must be JSON'}, 400
  {'message': "Password doesn't match"}, 400
  {'message': 'Email is not available'}, 400
  {'message': 'Username is not available'}, 400
  ````
__Edit user role:__\
API Endpoint: http://127.0.0.1:5000/edit_user_role/<user_id> \
Request data:
````
  token
  { "role": ""}
  ````
Response:
````
  If succeed: { "name": "", "email": "", "role": "" }, 200
  ````
````
  If failed: {'message': 'Request body must be JSON'}, 400
  {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'Access denied'}, 400
  {'message': 'No registered user'}, 400
  ````
__View all user info:__\
API Endpoint: http://127.0.0.1:5000/user_info \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'users': [{ "name": "", "email": "", "address": "", "role": "" }]}, 200
  ````
````
  If failed:{'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'Access denied'}, 400
  ````
__Add a ticket:__\
API Endpoint: http://127.0.0.1:5000/add_ticket \
Request data:
````
  token
  { "ticket_type": "", "name": "", "email": "", "subject": "", "message": "", "user_id": }
  ````
Response:
````
  If succeed: { "ticket_type": "", "name": "", "email": "", "subject": "", "message": "" , "user_id": }, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'Access denied'}, 400
  {'message': 'Request body must be JSON'}, 400
  ````
__View all user tickets:__\
API Endpoint: http://127.0.0.1:5000/ticket_info \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'tickets': [{ "ticket_type": "", "name": "", "email": "", "subject": "", message": ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket'}, 400
  ````
__Answer a ticket:__\
API Endpoint: http://127.0.0.1:5000/reply/<ticket_id> \
Request data:
````
  token
  { "reply_msg": "" }
  ````
Response:
````
  If succeed: {ticket_id': , 'sender': "", 'reply': ""}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket against this id'}, 400
  ````
__View all replied messages:__\
API Endpoint: http://127.0.0.1:5000/ticket_replies/<ticket_id> \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'message': [{'sender': "", 'message': "",'recipient': ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No reply yet!'}, 400
  ````

## Support Admin Panel

__Login__\
API Endpoint: http://127.0.0.1:5000/login \
Request data: Using basic authentication form
````
  Username
  Password
  ````
Response:
````
  If succeed: {'token': token_value}, 200
  ````
````
  If failed: {'message': 'Authorization is required'}, 400
  {'message': 'Username is required'}, 400
  {'message': 'Password is required'}, 400
  {'message': 'Please enter valid username and password'}, 400
  ````
__Add user:__\
API Endpoint: http://127.0.0.1:5000/register \
Request data:
````
  { "name": "", "email": "", "address": "", "username": "", "password": "", "confirm_password": "" }
  ````
Response:
````
  If succeed: { "email": "", "username": ""}, 200
  ````
````
  If failed: {'message': 'Request body must be JSON'}, 400
  {'message': "Password doesn't match"}, 400
  {'message': 'Email is not available'}, 400
  {'message': 'Username is not available'}, 400
  ````
__View all user info:__\
API Endpoint: http://127.0.0.1:5000/user_info \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'users': [{ "name": "", "email": "", "address": "", "role": "" }]}, 200
  ````
````
  If failed:{'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'Access denied'}, 400
  ````
__Add a ticket:__\
API Endpoint: http://127.0.0.1:5000/add_ticket \
Request data:
````
  token
  { "ticket_type": "", "name": "", "email": "", "subject": "", "message": "", "user_id": }
  ````
Response:
````
  If succeed: { "ticket_type": "", "name": "", "email": "", "subject": "", "message": "" , "user_id": }, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'Access denied'}, 400
  {'message': 'Request body must be JSON'}, 400
  ````
__View all user tickets:__\
API Endpoint: http://127.0.0.1:5000/ticket_info \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'tickets': [{ "ticket_type": "", "name": "", "email": "", "subject": "", message": ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket'}, 400
  ````
__Answer a ticket:__\
API Endpoint: http://127.0.0.1:5000/reply/<ticket_id> \
Request data:
````
  token
  { "reply_msg": "" }
  ````
Response:
````
  If succeed: {ticket_id': , 'sender': "", 'reply': ""}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket against this id'}, 400
  ````
__View all replied messages:__\
API Endpoint: http://127.0.0.1:5000/ticket_replies/<ticket_id> \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'message': [{'sender': "", 'message': "",'recipient': ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No reply yet!'}, 400
  ````  

## Support Person Panel

__Login:__\
API Endpoint: http://127.0.0.1:5000/login \
Request data: Using basic authentication form
````
  Username
  Password
  ````
Response:
````
  If succeed: {'token': token_value}, 200
  ````
````
  If failed: {'message': 'Authorization is required'}, 400
  {'message': 'Username is required'}, 400
  {'message': 'Password is required'}, 400
  {'message': 'Please enter valid username and password'}, 400
  ````
__View all user tickets:__\
API Endpoint: http://127.0.0.1:5000/ticket_info \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'tickets': [{ "ticket_type": "", "name": "", "email": "", "subject": "", message": ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket'}, 400
  ````
__Answer a ticket:__\
API Endpoint: http://127.0.0.1:5000/reply/<ticket_id> \
Request data:
````
  token
  { "reply_msg": "" }
  ````
Response:
````
  If succeed: {ticket_id': , 'sender': "", 'reply': ""}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket against this id'}, 400
  ````
__View all replied messages:__\
API Endpoint: http://127.0.0.1:5000/ticket_replies/<ticket_id> \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'message': [{'sender': "", 'message': "",'recipient': ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No reply yet!'}, 400
  ````
## Customer Panel

__Register:__\
API Endpoint: http://127.0.0.1:5000/register \
Request data:
````
  { "name": "", "email": "", "address": "", "username": "", "password": "", "confirm_password": "" }\
  ````
Response:
````
  If succeed: { "email": "", "username": ""}, 200
  ````
````
  If failed: {'message': 'Request body must be JSON'}, 400
  {'message': "Password doesn't match"}, 400
  {'message': 'Email is not available'}, 400
  {'message': 'Username is not available'}, 400
  ````
__Login:__\
API Endpoint: http://127.0.0.1:5000/login \
Request data: Using basic authentication form
````
  Username
  Password
  ````
Response:
````
  If succeed: {'token': token_value}, 200
  ````
````
  If failed: {'message': 'Authorization is required'}, 400
  {'message': 'Username is required'}, 400
  {'message': 'Password is required'}, 400
  {'message': 'Please enter valid username and password'}, 400
  ````
__Add a ticket:__\
API Endpoint: http://127.0.0.1:5000/add_ticket \
Request data:
````
  token
  { "ticket_type": "", "name": "", "email": "", "subject": "", "message": ""}
  ````
Response:
````
  If succeed: { "ticket_type": "", "name": "", "email": "", "subject": "", "message": "" , "user_id": }, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'Access denied'}, 400
  {'message': 'Request body must be JSON'}, 400
  ````
__View all tickets:__\
API Endpoint: http://127.0.0.1:5000/ticket_info \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'tickets': [{ "ticket_type": "", "name": "", "email": "", "subject": "", message": ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket'}, 400
  ````
__Answer a ticket:__\
API Endpoint: http://127.0.0.1:5000/reply/<ticket_id> \
Request data:
````
  token
  { "reply_msg": "" }
  ````
Response:
````
  If succeed: {ticket_id': , 'sender': "", 'reply': ""}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No opened ticket against this id'}, 400
  ````
__View all replied messages:__\
API Endpoint: http://127.0.0.1:5000/ticket_replies/<ticket_id> \
Request data:
````
  token
  ````
Response:
````
  If succeed: {'message': [{'sender': "", 'message': "",'recipient': ""}]}, 200
  ````
````
  If failed: {'message' : 'Token is missing!!'}, 401
  {'message': 'Token is invalid!!'}, 401
  {'message': 'No reply yet!'}, 400
  ````
