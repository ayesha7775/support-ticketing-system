from flask_sqlalchemy import SQLAlchemy
from app import app

app.config['SQLALCHEMY_DATABASE_URI']= 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=True)
    email = db.Column(db.String(60), nullable=False)
    address = db.Column(db.String(80), nullable=False)
    username = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(50), nullable=False)
    role = db.Column(db.String(15), nullable=False, default="Customer")

class Ticket_info(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ticket_type = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(60), nullable=False)
    sub = db.Column(db.String(50), nullable=False)
    message = db.Column(db.String(80), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

class Reply_info(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender = db.Column(db.String(60), nullable=False)
    message = db.Column(db.String(80), nullable=False)
    recipient = db.Column(db.String(60), nullable=True)
    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket_info.id'))


# db.create_all()
